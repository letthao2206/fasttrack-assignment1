package com.thaolt15.assignment1.constant;

public class CommonConstants {
    private CommonConstants() {
    }

    public static final String ROLE_ADMIN = "ADMIN";
    public static final String ROLE_MEMBER = "MEMBER";
}
