package com.thaolt15.assignment1.constant;

public class PermissionConstant {
    private PermissionConstant() {
    }

    public static final String VIEW = "VIEW";
    public static final String DOWNLOAD = "DOWNLOAD";
}
