package com.thaolt15.assignment1.converter;

import com.thaolt15.assignment1.dto.SharedFileDTO;
import com.thaolt15.assignment1.entity.SharedFileEntity;

public class SharedFileDTOConverter {

    public static SharedFileDTO createSharedFileDTO(SharedFileEntity entity){
        return SharedFileDTO.builder().filename(entity.getSharedFile().getFileName()).username(entity.getSharedUser().getUsername()).permission(entity.getPermission()).build();
    }
}
