package com.thaolt15.assignment1.converter;

import com.thaolt15.assignment1.entity.UserEntity;
import com.thaolt15.assignment1.exception.DataNotFoundException;
import com.thaolt15.assignment1.dto.response.UserResponse;

public class UserDTOConverter {

    public static UserResponse createResponse(UserEntity userEntity) {
        UserResponse _userResponse = new UserResponse();
        if(userEntity == null){
            //TODO: delete or modify
            throw new DataNotFoundException();
        } else {
            _userResponse.setUsername(userEntity.getUsername());
        }
        return _userResponse;
    }
}
