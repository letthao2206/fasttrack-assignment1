package com.thaolt15.assignment1.dto;

import com.thaolt15.assignment1.constant.Permission;
import lombok.*;

import java.util.Set;

@Setter@Getter@AllArgsConstructor@NoArgsConstructor@Builder
public class SharedFileDTO {

    private String username;
    private String filename;
    private String downloadUrl;
    private Permission permission;
}
