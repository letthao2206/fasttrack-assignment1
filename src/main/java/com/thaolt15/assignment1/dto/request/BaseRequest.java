package com.thaolt15.assignment1.dto.request;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class BaseRequest {

    private LocalDateTime createdDate;

}
