package com.thaolt15.assignment1.dto.request;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@Setter@Getter@AllArgsConstructor@NoArgsConstructor
@Builder
public class FileRequest extends BaseRequest {

    MultipartFile fileData;
    String owner;
    Long size;
    String storageUri;

}
