package com.thaolt15.assignment1.dto.request;

import lombok.*;

import javax.xml.bind.annotation.XmlRootElement;

@AllArgsConstructor@NoArgsConstructor
@Setter@Getter@Builder
@XmlRootElement
public class UrlRequest {

    private String username;
    private String originalUrl;
}
