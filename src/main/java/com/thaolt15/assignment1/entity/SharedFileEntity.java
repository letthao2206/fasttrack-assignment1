package com.thaolt15.assignment1.entity;

import com.thaolt15.assignment1.constant.Permission;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name="SharedFile", schema = "Filesystem")
@Setter@Getter@NoArgsConstructor
public class SharedFileEntity extends BaseEntity{

    @ManyToOne
    @JoinColumn(name = "shared_user", referencedColumnName = "username")
    private UserEntity sharedUser;

    @ManyToOne
    @JoinColumn(name = "shared_file", referencedColumnName = "file_name")
    private FileEntity sharedFile;

    @Enumerated(EnumType.STRING)
    private Permission permission;

    @Builder
    private SharedFileEntity(LocalDateTime createdAt, Long id, UserEntity sharedUser, FileEntity sharedFile, Permission permission) {
        super(createdAt, id);
        this.sharedUser = sharedUser;
        this.sharedFile = sharedFile;
        this.permission = permission;
    }
}
