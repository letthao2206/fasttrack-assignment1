package com.thaolt15.assignment1.repository;

import com.thaolt15.assignment1.entity.FileEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FileRepository extends CrudRepository<FileEntity, Long> {

    List<FileEntity> findFileEntitiesByUserEntityUsername(String username);
    FileEntity findFileEntityByUserEntityUsernameAndFileName(String username, String filename);
    FileEntity findFileEntityByFileName(String filename);
    List<FileEntity> findFileEntityByFileNameContainsOrUserEntity_Username(String filename);
    FileEntity findByDownloadUri(String downloadUri);
}
