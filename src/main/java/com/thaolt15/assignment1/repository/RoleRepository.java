package com.thaolt15.assignment1.repository;

import com.thaolt15.assignment1.entity.RoleEntity;
import org.springframework.data.repository.CrudRepository;


public interface RoleRepository  extends CrudRepository<RoleEntity, Long> {

    RoleEntity findRoleEntityByName(String name);
}
