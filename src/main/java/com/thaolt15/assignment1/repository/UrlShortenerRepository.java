package com.thaolt15.assignment1.repository;

import com.thaolt15.assignment1.entity.URLEntity;
import org.springframework.data.repository.CrudRepository;

public interface UrlShortenerRepository extends CrudRepository<URLEntity, Long> {

    URLEntity findByShortenURL(String shortenUrl);
}
