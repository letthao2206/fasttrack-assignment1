package com.thaolt15.assignment1.repository;

import com.thaolt15.assignment1.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<UserEntity, Long> {

    Optional<UserEntity> findUserEntityByUsername(String username);
    List<Optional<UserEntity>> findUserEntityByUsernameContaining(String username);
}
