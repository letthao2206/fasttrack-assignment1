package com.thaolt15.assignment1.service;

import com.thaolt15.assignment1.dto.request.UrlRequest;
import com.thaolt15.assignment1.entity.URLEntity;

import javax.servlet.http.HttpServletRequest;

public interface UrlShortenerService {

    String shortenURL(UrlRequest urlRequest, HttpServletRequest request);
    URLEntity saveURL(URLEntity entity);
    String findOriginalUrlByShortenedUrl(String shortenedUrl);
}
