package com.thaolt15.assignment1.service;

import com.thaolt15.assignment1.dto.UserDTO;
import com.thaolt15.assignment1.dto.response.UserResponse;
import com.thaolt15.assignment1.exception.DuplicateException;
import org.springframework.security.core.userdetails.User;

public interface UserService {

    UserResponse findUserByUsername(String username);
    UserResponse registerNewUserAccount(UserDTO userDto) throws DuplicateException;
}
