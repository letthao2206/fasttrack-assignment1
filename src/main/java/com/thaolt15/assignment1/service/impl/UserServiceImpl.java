package com.thaolt15.assignment1.service.impl;

import com.thaolt15.assignment1.converter.UserDTOConverter;
import com.thaolt15.assignment1.dto.UserDTO;
import com.thaolt15.assignment1.dto.response.UserResponse;
import com.thaolt15.assignment1.entity.UserEntity;
import com.thaolt15.assignment1.exception.DuplicateException;
import com.thaolt15.assignment1.repository.UserRepository;
import com.thaolt15.assignment1.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository repository;

    @Override
    public UserResponse findUserByUsername(String username) {
        UserEntity user = repository.findUserEntityByUsernameContaining(username).iterator().next().orElse(new UserEntity());
        return UserDTOConverter.createResponse(user);
    }

    @Override
    public UserResponse registerNewUserAccount(UserDTO userDto) throws DuplicateException {
        UserEntity entity = repository.findUserEntityByUsername(userDto.getEmail()).orElse(null);

        if(entity != null) {
            throw new DuplicateException("User exists!");
        }

        return UserDTOConverter.createResponse(entity);

    }

}
