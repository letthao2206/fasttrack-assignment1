package com.thaolt15.assignment1.validator;

import com.thaolt15.assignment1.exception.FileNotFoundException;
import org.springframework.security.core.userdetails.User;

public class FileValidator {

    private FileValidator(){ }

    public static boolean checkFilePermission(User user,  String username){
        return username.equals(user.getUsername());
    }

    public static boolean isValidFilename(String filename) {
        return filename.contains("..");
    }
}
