IF NOT EXISTS(SELECT * FROM sys.databases WHERE name = 'FastTrack')
  BEGIN
    CREATE DATABASE FastTrack
    END
    GO
       USE FastTrack
    GO


USE FastTrack;
IF (NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'Assignment1'))
BEGIN
    EXEC ('CREATE SCHEMA Assignment1 AUTHORIZATION [dbo]')
END

DROP TABLE Assignment1.USER_TBL;
IF NOT EXISTS(SELECT * FROM sysobjects WHERE name = 'USER_MST')
  BEGIN
	CREATE TABLE Assignment1.file_tbl (
	fid bigint not null,
	created_at DATETIME2,
	file_name VARCHAR(255),
	file_size BIGINT,
	ownload_uri VARCHAR(255),
	username VARCHAR(70) UNIQUE NOT NULL,
	PRIMARY KEY (fid) );
	END

DROP TABLE Assignment1.FILE_TBL;
IF NOT EXISTS(SELECT * FROM sysobjects WHERE name = 'FILE_MST')
  BEGIN
	CREATE TABLE Assignment1.user_tbl (
	uid BIGINT NOT NULL,
	created_at DATETIME2,
	password VARCHAR(255),
	role VARCHAR(10),
	username VARCHAR(70) UNIQUE NOT NULL,
	PRIMARY KEY (uid));
	END


create table filesystem.url_sharing_tbl (username varchar(70) not null, shortenurl varchar(255) not null, primary key (username, shortenurl));

ALTER TABLE Assignment1.file_tbl ADD CONSTRAINT FK FOREIGN KEY (username) REFERENCES Assignment1.user_tbl (username);