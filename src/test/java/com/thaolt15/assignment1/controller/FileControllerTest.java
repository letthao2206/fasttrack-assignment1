package com.thaolt15.assignment1.controller;

import com.thaolt15.assignment1.dto.response.FileResponse;
import com.thaolt15.assignment1.service.impl.FileServiceImpl;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RunWith(SpringRunner.class)
@WebMvcTest(value = FileController.class)
@WithMockUser
@NoArgsConstructor
public class FileControllerTest {

    @Autowired
    private MockMvc mockMvc;


    @MockBean
    private FileServiceImpl fileService;

    @Test
    public void retrieveAllFilesPositive() throws Exception {
        String user = "thaolt15";

        List<FileResponse> list = new ArrayList<>();
        list.add(new FileResponse("4.jpg", user, 204997l, "http://localhost:8080/downloadFile/4.jpg"));
        list.add(new FileResponse("5c2c0d02d3e211a644202fca75ff4e88.jpg", user, 21385l,"http://localhost:8080/downloadFile/5c2c0d02d3e211a644202fca75ff4e88.jpg" ));

        Mockito.when(
                fileService.findFilesByUsername(Mockito.anyString())).thenReturn(list);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                "/assignment1/retrieveAllFiles?username=thaolt15").accept(
                MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

    }

    @Test
    public void retrieveAllFilesUnauthorized() throws Exception {
        String token = "token";

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                "/assignment1/retrieveAllFiles?username=thao").header("Authorization", token).accept(
                MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        String expected = "{\"status\":\"Failed\",\"message\":\"Unauthorized\"}";

        JSONAssert.assertEquals(expected, result.getResponse()
                .getContentAsString(), false);
    }

    @Test
    public void uploadFilePositive() throws Exception{
        String user = "thaolt15";

        MockMultipartFile multipartFile = new MockMultipartFile("D:\\ffmpeg\\LICENSE.txt", "D:\\ffmpeg\\LICENSE.txt",
                "multipart/form-data", "LICENSE.txt".getBytes());

        String token = "token";

        mockMvc.perform(MockMvcRequestBuilders
                .multipart("/assignment1/uploadFile").file(multipartFile).characterEncoding("UTF-8")
                .header("Authorization", token))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    public void uploadFileUnauthorizedNegative() throws Exception{

        MockMultipartFile multipartFile = new MockMultipartFile("D:\\ffmpeg\\LICENSE.txt", "D:\\ffmpeg\\LICENSE.txt",
                "multipart/form-data", "LICENSE.txt".getBytes());
        mockMvc.perform(MockMvcRequestBuilders
                .multipart("/assignment1/uploadFile").file(multipartFile).characterEncoding("UTF-8")
                .header("Authorization", "token"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }


}