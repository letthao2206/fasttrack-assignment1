package com.thaolt15.assignment1.service;

import com.thaolt15.assignment1.config.FileConfigProperties;
import com.thaolt15.assignment1.converter.FileDTOConverter;
import com.thaolt15.assignment1.entity.FileEntity;
import com.thaolt15.assignment1.entity.UserEntity;
import com.thaolt15.assignment1.exception.DataNotFoundException;
import com.thaolt15.assignment1.exception.DuplicateException;
import com.thaolt15.assignment1.repository.FileRepository;
import com.thaolt15.assignment1.dto.response.FileResponse;
import com.thaolt15.assignment1.repository.UserRepository;
import com.thaolt15.assignment1.service.impl.FileServiceImpl;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@Slf4j
@RunWith(MockitoJUnitRunner.class)
@NoArgsConstructor
public class FileServiceTest {

    @MockBean
    FileServiceImpl fileService;

    @Mock
    private FileRepository fileRepository;

    @Mock
    private UserRepository userRepository;

    FileConfigProperties fileConfigProperties;

    UserEntity user;
    FileEntity file;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        fileConfigProperties = new FileConfigProperties();
        fileConfigProperties.setFileUri("/Documents");
//        fileService = new FileServiceImpl(fileConfigProperties, fileRepository, userRepository);
    }

    @Test
    public void saveFilePositive() {
        given(fileRepository.save(file)).willReturn(file);

        FileResponse _response = fileService.save(file);

        assertNotNull(_response);
        assertEquals(file.getFileName(), _response.getFileName());
        assertEquals(file.getUserEntity().getUsername(), _response.getOwner());
        assertEquals(file.getDownloadUri(), _response.getDownloadUri());
    }

    @Test
    public void saveFileNegativeWithDuplicateException() {
        given(fileRepository.findFileEntityByUserEntityUsernameAndFileName("thao", "f")).willReturn(file);

        assertThrows(DuplicateException.class, () -> fileService.save(file));
    }

    @Test
    public void saveFileNegativeWithDataNotFoundException() {
        assertThrows(DataNotFoundException.class, () -> fileService.save(file));
    }

    @Test
    public void findFileEntityByUserEntityUsernameAndFileNameNegative() {
        assertThrows(DataNotFoundException.class, () -> fileService.findFileByUsernameAndFilename("thao", "f"));
    }

    @Test
    public void findFileEntityByUserEntityUsernameAndFileNamePositive() {
        given(fileRepository.findFileEntityByUserEntityUsernameAndFileName("thao", "f")).willReturn(file);

        FileResponse _response = fileService.findFileByUsernameAndFilename("thao", "f");

        assertNotNull(_response);
        assertEquals(file.getFileName(), _response.getFileName());
        assertEquals(file.getUserEntity().getUsername(), _response.getOwner());
        assertEquals(file.getDownloadUri(), _response.getDownloadUri());
    }

    @Test
    public void createFileResponsePositive(){
        FileResponse _response = FileDTOConverter.createFileResponse(file);

        assertEquals(_response.getFileName(), file.getFileName());
        assertEquals(_response.getOwner(), file.getUserEntity().getUsername());
        assertEquals(_response.getDownloadUri(), file.getDownloadUri());
    }

    @Test
    public void storeFilePositive(){
        MockMultipartFile mockMultipartFile = new MockMultipartFile("data", "filename.txt", "text/plain", "some xml".getBytes() );
    }

}
