package com.thaolt15.assignment1.service;

import com.thaolt15.assignment1.entity.UserEntity;
import com.thaolt15.assignment1.exception.DataNotFoundException;
import com.thaolt15.assignment1.repository.UserRepository;
import com.thaolt15.assignment1.dto.response.UserResponse;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import static org.mockito.BDDMockito.*;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@RunWith(MockitoJUnitRunner.class)
@NoArgsConstructor
class UserServiceTest {

}